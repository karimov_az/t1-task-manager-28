package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractFieldException {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) throws AbstractFieldException {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
