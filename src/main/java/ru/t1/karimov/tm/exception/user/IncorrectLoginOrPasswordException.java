package ru.t1.karimov.tm.exception.user;

public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login/password entered. Please try again...");
    }

}
