package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[PROJECT LIST BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id.";
    }

}
