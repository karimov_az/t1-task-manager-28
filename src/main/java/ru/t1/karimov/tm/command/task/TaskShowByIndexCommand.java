package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[TASK LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

}
