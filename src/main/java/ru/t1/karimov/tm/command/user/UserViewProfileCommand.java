package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile of current user.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
