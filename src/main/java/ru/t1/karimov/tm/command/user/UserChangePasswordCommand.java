package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand{

    @Override
    public void execute() throws AbstractException {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
