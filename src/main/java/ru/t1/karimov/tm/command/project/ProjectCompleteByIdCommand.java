package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

}
