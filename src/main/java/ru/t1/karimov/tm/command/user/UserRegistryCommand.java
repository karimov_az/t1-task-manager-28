package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registry user.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
